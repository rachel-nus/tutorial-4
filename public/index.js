"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

// Defaults
var MAX_WAITING = 25;
var defaultState = {
  customers: [],
  maxSn: 0,
  numFreeSlots: MAX_WAITING,
  showWaitlist: false
};
var pinkContainerClassName = "w3-container w3-padding-16 w3-pale-red w3-center w3-wide";
var genericButtonClassName = "w3-button w3-round w3-red w3-opacity w3-hover-opacity-off";
var closeButtonClassName = "w3-button w3-round w3-green w3-opacity w3-hover-opacity-off";
var deleteButtonClassName = "w3-button w3-round w3-grey w3-opacity w3-hover-opacity-off";

var AddCustomer = /*#__PURE__*/function (_React$Component) {
  _inherits(AddCustomer, _React$Component);

  var _super = _createSuper(AddCustomer);

  function AddCustomer() {
    var _this;

    _classCallCheck(this, AddCustomer);

    _this = _super.call(this);
    _this.state = {
      showForm: false
    };
    _this.hideForm = _this.hideForm.bind(_assertThisInitialized(_this));
    return _this;
  }

  _createClass(AddCustomer, [{
    key: "hideForm",
    value: function hideForm() {
      this.setState({
        showForm: !this.state.showForm
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var showFormStatus = this.state.showForm;
      return /*#__PURE__*/React.createElement("div", {
        className: pinkContainerClassName,
        id: "addCustomer"
      }, /*#__PURE__*/React.createElement("button", {
        className: showFormStatus ? closeButtonClassName : genericButtonClassName,
        onClick: function onClick() {
          return _this2.hideForm();
        }
      }, showFormStatus ? "Close" : "Add Customer"), showFormStatus && /*#__PURE__*/React.createElement(CustomerForm, {
        createCustomerOnClick: this.props.createCustomerOnClick,
        numFreeSlots: this.props.numFreeSlots
      }));
    }
  }]);

  return AddCustomer;
}(React.Component);

var CustomerForm = /*#__PURE__*/function (_React$Component2) {
  _inherits(CustomerForm, _React$Component2);

  var _super2 = _createSuper(CustomerForm);

  function CustomerForm(props) {
    var _this3;

    _classCallCheck(this, CustomerForm);

    _this3 = _super2.call(this, props);
    _this3.state = {
      customerName: "",
      phoneNumber: "",
      createdTime: new Date(),
      formErrors: {
        firstError: "",
        secondError: ""
      },
      customerNameIsValid: false,
      phoneNumberIsValid: false
    };
    _this3.handleSubmit = _this3.handleSubmit.bind(_assertThisInitialized(_this3));
    _this3.handleNameChange = _this3.handleNameChange.bind(_assertThisInitialized(_this3));
    _this3.handlePhoneNumberChange = _this3.handlePhoneNumberChange.bind(_assertThisInitialized(_this3));
    return _this3;
  }

  _createClass(CustomerForm, [{
    key: "handleSubmit",
    value: function handleSubmit(e) {
      e.preventDefault();

      if (this.state.customerNameIsValid & this.state.phoneNumberIsValid) {
        if (this.props.numFreeSlots > 0) {
          var currentDate = new Date(Date.now());
          var customer = {
            customerName: this.state.customerName,
            phoneNumber: this.state.phoneNumber,
            createdTime: currentDate.toLocaleString()
          };
          this.props.createCustomerOnClick(customer);
          this.setState({
            customerName: "",
            phoneNumber: ""
          });
        } else {
          window.alert("sorry, there's no more space left on the waiting list!");
        }
      } else {
        window.alert("please fix the form first!");
      }
    }
  }, {
    key: "handleNameChange",
    value: function handleNameChange(e) {
      var customerNameInput = e.target.value;
      var fieldValidationErrors = this.state.formErrors;

      if (!/^[a-zA-Z ]+$/.test(customerNameInput)) {
        fieldValidationErrors.firstError = "customer name should only contain letters!";
        this.setState({
          customerNameIsValid: false
        });
      } else {
        fieldValidationErrors.firstError = "";
        this.setState({
          customerNameIsValid: true
        });
      }

      this.setState({
        customerName: customerNameInput
      });
    }
  }, {
    key: "handlePhoneNumberChange",
    value: function handlePhoneNumberChange(e) {
      var phoneNumberInput = e.target.value;
      var fieldValidationErrors = this.state.formErrors;

      if (!/^\d+$/.test(phoneNumberInput)) {
        fieldValidationErrors.secondError = "phone number should only contain numbers!";
        this.setState({
          phoneNumberIsValid: false
        });
      } else if (phoneNumberInput.length != 8) {
        fieldValidationErrors.secondError = "phone number should be 8 numbers long!";
        this.setState({
          phoneNumberIsValid: false
        });
      } else {
        fieldValidationErrors.secondError = "";
        this.setState({
          phoneNumberIsValid: true
        });
      }

      this.setState({
        phoneNumber: phoneNumberInput
      });
    }
  }, {
    key: "render",
    value: function render() {
      return /*#__PURE__*/React.createElement("div", {
        className: pinkContainerClassName
      }, /*#__PURE__*/React.createElement("form", {
        name: "addCustForm",
        onSubmit: this.handleSubmit
      }, /*#__PURE__*/React.createElement("label", {
        htmlFor: "custName"
      }, /*#__PURE__*/React.createElement("b", null, " Name:")), /*#__PURE__*/React.createElement("input", {
        type: "text",
        id: "custName",
        name: "custName",
        onChange: this.handleNameChange,
        value: this.state.customerName
      }), /*#__PURE__*/React.createElement("div", {
        style: {
          color: "red"
        }
      }, this.state.formErrors.firstError), /*#__PURE__*/React.createElement("br", null), /*#__PURE__*/React.createElement("br", null), /*#__PURE__*/React.createElement("label", {
        htmlFor: "custPhone"
      }, /*#__PURE__*/React.createElement("b", null, " Phone Number:")), /*#__PURE__*/React.createElement("input", {
        type: "text",
        id: "custPhone",
        name: "custPhone",
        onChange: this.handlePhoneNumberChange,
        value: this.state.phoneNumber
      }), /*#__PURE__*/React.createElement("div", {
        style: {
          color: "red"
        }
      }, this.state.formErrors.secondError), /*#__PURE__*/React.createElement("br", null), /*#__PURE__*/React.createElement("br", null), /*#__PURE__*/React.createElement("button", {
        className: genericButtonClassName
      }, "Add Customer to Waitlist")));
    }
  }]);

  return CustomerForm;
}(React.Component);

var DeleteCustomer = /*#__PURE__*/function (_React$Component3) {
  _inherits(DeleteCustomer, _React$Component3);

  var _super3 = _createSuper(DeleteCustomer);

  function DeleteCustomer(props) {
    var _this4;

    _classCallCheck(this, DeleteCustomer);

    _this4 = _super3.call(this, props);
    _this4.handleSubmit = _this4.handleSubmit.bind(_assertThisInitialized(_this4));
    return _this4;
  }

  _createClass(DeleteCustomer, [{
    key: "handleSubmit",
    value: function handleSubmit(e) {
      var customer = this.props.customer;
      this.props.deleteCustomerOnClick(customer);
    }
  }, {
    key: "render",
    value: function render() {
      return /*#__PURE__*/React.createElement("button", {
        className: deleteButtonClassName,
        onClick: this.handleSubmit
      }, /*#__PURE__*/React.createElement("i", {
        className: "fa fa-close"
      }));
    }
  }]);

  return DeleteCustomer;
}(React.Component);

var DisplayCustomers = /*#__PURE__*/function (_React$Component4) {
  _inherits(DisplayCustomers, _React$Component4);

  var _super4 = _createSuper(DisplayCustomers);

  function DisplayCustomers() {
    _classCallCheck(this, DisplayCustomers);

    return _super4.apply(this, arguments);
  }

  _createClass(DisplayCustomers, [{
    key: "render",
    value: function render() {
      var _this5 = this;

      var showWaitlist = this.props.showWaitlistStatus;
      return /*#__PURE__*/React.createElement("div", {
        className: pinkContainerClassName,
        id: "viewWaitlist"
      }, showWaitlist && /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("h3", null, "The Waitlist"), /*#__PURE__*/React.createElement(WaitListTable, {
        customers: this.props.customers,
        deleteCustomerOnClick: this.props.deleteCustomerOnClick
      })), /*#__PURE__*/React.createElement("button", {
        className: showWaitlist ? closeButtonClassName : genericButtonClassName,
        onClick: function onClick() {
          return _this5.props.showCustomersOnClick();
        }
      }, showWaitlist ? "Return to Homepage" : "View Waitlist"));
    }
  }]);

  return DisplayCustomers;
}(React.Component);

var DisplayFreeSlots = /*#__PURE__*/function (_React$Component5) {
  _inherits(DisplayFreeSlots, _React$Component5);

  var _super5 = _createSuper(DisplayFreeSlots);

  function DisplayFreeSlots() {
    _classCallCheck(this, DisplayFreeSlots);

    return _super5.apply(this, arguments);
  }

  _createClass(DisplayFreeSlots, [{
    key: "render",
    value: function render() {
      return /*#__PURE__*/React.createElement("div", {
        className: pinkContainerClassName
      }, "Number of Free Slots: ", this.props.numFreeSlots);
    }
  }]);

  return DisplayFreeSlots;
}(React.Component);

var ClearWaitList = /*#__PURE__*/function (_React$Component6) {
  _inherits(ClearWaitList, _React$Component6);

  var _super6 = _createSuper(ClearWaitList);

  function ClearWaitList() {
    _classCallCheck(this, ClearWaitList);

    return _super6.apply(this, arguments);
  }

  _createClass(ClearWaitList, [{
    key: "render",
    value: function render() {
      var _this6 = this;

      return /*#__PURE__*/React.createElement("div", {
        className: pinkContainerClassName,
        id: "clearWaitList"
      }, /*#__PURE__*/React.createElement("button", {
        className: genericButtonClassName,
        onClick: function onClick() {
          return _this6.props.clearWaitlistOnClick();
        }
      }, "Clear Waitlist"));
    }
  }]);

  return ClearWaitList;
}(React.Component);

var Header = /*#__PURE__*/function (_React$Component7) {
  _inherits(Header, _React$Component7);

  var _super7 = _createSuper(Header);

  function Header() {
    _classCallCheck(this, Header);

    return _super7.apply(this, arguments);
  }

  _createClass(Header, [{
    key: "render",
    value: function render() {
      return /*#__PURE__*/React.createElement("div", {
        id: "homepage"
      }, /*#__PURE__*/React.createElement("div", {
        className: "w3-container w3-padding-16 w3-pale-blue w3-center w3-wide"
      }, /*#__PURE__*/React.createElement("h1", {
        className: "w3-text-grey"
      }, /*#__PURE__*/React.createElement("b", null, "Hotel California Waitlist System"))));
    }
  }]);

  return Header;
}(React.Component);

var DisplayHomepage = /*#__PURE__*/function (_React$Component8) {
  _inherits(DisplayHomepage, _React$Component8);

  var _super8 = _createSuper(DisplayHomepage);

  function DisplayHomepage() {
    _classCallCheck(this, DisplayHomepage);

    return _super8.apply(this, arguments);
  }

  _createClass(DisplayHomepage, [{
    key: "render",
    value: function render() {
      return /*#__PURE__*/React.createElement("div", {
        id: "homepage"
      }, /*#__PURE__*/React.createElement(Header, null), /*#__PURE__*/React.createElement("br", null), /*#__PURE__*/React.createElement(DisplayFreeSlots, {
        numFreeSlots: this.props.numFreeSlots
      }), /*#__PURE__*/React.createElement("br", null), /*#__PURE__*/React.createElement(AddCustomer, {
        createCustomerOnClick: this.props.createCustomerOnClick,
        numFreeSlots: this.props.numFreeSlots
      }), /*#__PURE__*/React.createElement("br", null), /*#__PURE__*/React.createElement(DisplayCustomers, {
        customers: this.props.customers,
        showCustomersOnClick: this.props.showCustomersOnClick,
        deleteCustomerOnClick: this.props.deleteCustomerOnClick,
        showWaitlistStatus: this.props.showWaitlistStatus
      }), /*#__PURE__*/React.createElement("br", null), /*#__PURE__*/React.createElement(ClearWaitList, {
        clearWaitlistOnClick: this.props.clearWaitlistOnClick
      }));
    }
  }]);

  return DisplayHomepage;
}(React.Component);

function WaitListRow(props) {
  var customerProp = props.customer;
  return /*#__PURE__*/React.createElement("tr", null, /*#__PURE__*/React.createElement("td", null, customerProp.id), /*#__PURE__*/React.createElement("td", null, customerProp.customerName), /*#__PURE__*/React.createElement("td", null, customerProp.phoneNumber), /*#__PURE__*/React.createElement("td", null, customerProp.createdTime), /*#__PURE__*/React.createElement("td", null, /*#__PURE__*/React.createElement(DeleteCustomer, {
    customer: customerProp,
    deleteCustomerOnClick: props.deleteCustomerOnClick
  })));
}

var WaitListTable = /*#__PURE__*/function (_React$Component9) {
  _inherits(WaitListTable, _React$Component9);

  var _super9 = _createSuper(WaitListTable);

  function WaitListTable() {
    _classCallCheck(this, WaitListTable);

    return _super9.apply(this, arguments);
  }

  _createClass(WaitListTable, [{
    key: "render",
    value: function render() {
      var _this7 = this;

      var customers = this.props.customers;
      var waitListRows = customers.map(function (customer) {
        return /*#__PURE__*/React.createElement(WaitListRow, {
          key: customer.id,
          customer: customer,
          deleteCustomerOnClick: _this7.props.deleteCustomerOnClick
        });
      });
      return /*#__PURE__*/React.createElement("div", {
        className: pinkContainerClassName
      }, /*#__PURE__*/React.createElement("table", null, /*#__PURE__*/React.createElement("thead", null, /*#__PURE__*/React.createElement("tr", null, /*#__PURE__*/React.createElement("th", null, "ID"), /*#__PURE__*/React.createElement("th", null, "Customer Name"), /*#__PURE__*/React.createElement("th", null, "Phone Number"), /*#__PURE__*/React.createElement("th", null, "Created Time"))), /*#__PURE__*/React.createElement("tbody", null, waitListRows)));
    }
  }]);

  return WaitListTable;
}(React.Component);

var SidebarNav = /*#__PURE__*/function (_React$Component10) {
  _inherits(SidebarNav, _React$Component10);

  var _super10 = _createSuper(SidebarNav);

  function SidebarNav() {
    _classCallCheck(this, SidebarNav);

    return _super10.apply(this, arguments);
  }

  _createClass(SidebarNav, [{
    key: "render",
    value: function render() {
      return /*#__PURE__*/React.createElement("div", {
        id: "sidebarNav",
        className: "w3-sidebar w3-light-grey w3-bar-block"
      }, /*#__PURE__*/React.createElement("h3", {
        className: "w3-bar-item"
      }, /*#__PURE__*/React.createElement("b", null, "\u2630 Menu")), /*#__PURE__*/React.createElement("a", {
        href: "#homepage",
        className: "w3-bar-item w3-button"
      }, "Homepage"), /*#__PURE__*/React.createElement("a", {
        href: "#addCustomer",
        className: "w3-bar-item w3-button"
      }, "Add Customer"), /*#__PURE__*/React.createElement("a", {
        href: "#viewWaitlist",
        className: "w3-bar-item w3-button"
      }, "View Waitlist"), /*#__PURE__*/React.createElement("a", {
        href: "#clearWaitList",
        className: "w3-bar-item w3-button"
      }, "Clear Waitlist"));
    }
  }]);

  return SidebarNav;
}(React.Component);

var App = /*#__PURE__*/function (_React$Component11) {
  _inherits(App, _React$Component11);

  var _super11 = _createSuper(App);

  function App() {
    var _this8;

    _classCallCheck(this, App);

    _this8 = _super11.call(this);
    _this8.state = JSON.parse(window.localStorage.getItem("state"));

    if (_this8.state === null) {
      _this8.state = defaultState;
    }

    _this8.handleCreateCustomer = _this8.handleCreateCustomer.bind(_assertThisInitialized(_this8));
    _this8.handleClearWaitlist = _this8.handleClearWaitlist.bind(_assertThisInitialized(_this8));
    _this8.handleDeleteCustomer = _this8.handleDeleteCustomer.bind(_assertThisInitialized(_this8));
    _this8.handleShowCustomers = _this8.handleShowCustomers.bind(_assertThisInitialized(_this8));
    return _this8;
  }

  _createClass(App, [{
    key: "setState",
    value: function setState(state) {
      window.localStorage.setItem("state", JSON.stringify(state));

      _get(_getPrototypeOf(App.prototype), "setState", this).call(this, state);
    }
  }, {
    key: "handleCreateCustomer",
    value: function handleCreateCustomer(customer) {
      var newmaxSn = this.state.maxSn + 1;
      customer.id = newmaxSn;
      var newCustomerList = this.state.customers.slice();
      newCustomerList.push(customer);
      var newNumFreeSlots = this.state.numFreeSlots - 1;
      this.setState({
        customers: newCustomerList,
        maxSn: newmaxSn,
        numFreeSlots: newNumFreeSlots,
        showWaitlist: this.state.showWaitlist
      });
    }
  }, {
    key: "handleClearWaitlist",
    value: function handleClearWaitlist(e) {
      window.localStorage.clear();
      this.setState(defaultState);
    }
  }, {
    key: "handleDeleteCustomer",
    value: function handleDeleteCustomer(customer) {
      var newCustomers = this.state.customers.filter(function (item) {
        return item.id !== customer.id;
      });
      var newNumFreeSlots = this.state.numFreeSlots + 1;
      this.setState({
        customers: newCustomers,
        maxSn: this.state.maxSn,
        numFreeSlots: newNumFreeSlots,
        showWaitlist: this.state.showWaitlist
      });
    }
  }, {
    key: "handleShowCustomers",
    value: function handleShowCustomers() {
      this.setState({
        customers: this.state.customers,
        maxSn: this.state.maxSn,
        numFreeSlots: this.state.numFreeSlots,
        showWaitlist: !this.state.showWaitlist
      });
    }
  }, {
    key: "render",
    value: function render() {
      return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", {
        className: "bgimg"
      }, /*#__PURE__*/React.createElement(SidebarNav, null), /*#__PURE__*/React.createElement("div", {
        id: "pageContent"
      }, !this.state.showWaitlist && /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(DisplayHomepage, {
        customers: this.state.customers,
        numFreeSlots: this.state.numFreeSlots,
        createCustomerOnClick: this.handleCreateCustomer,
        clearWaitlistOnClick: this.handleClearWaitlist,
        showCustomersOnClick: this.handleShowCustomers,
        deleteCustomerOnClick: this.handleDeleteCustomer,
        showWaitlist: this.state.showWaitlist
      })), this.state.showWaitlist && /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(Header, null), /*#__PURE__*/React.createElement("br", null), /*#__PURE__*/React.createElement(DisplayCustomers, {
        customers: this.state.customers,
        showCustomersOnClick: this.handleShowCustomers,
        deleteCustomerOnClick: this.handleDeleteCustomer,
        showWaitlistStatus: this.state.showWaitlist
      })))));
    }
  }]);

  return App;
}(React.Component);

var element = /*#__PURE__*/React.createElement(App, null);
ReactDOM.render(element, document.getElementById("contents"));